
video_folder=/home/pi/lspiv/169.254.175.180
camera1=169.254.175.180
# http://192.168.1.64/
#-------------------------------------------------------------------------------
# Station number. Used as a prefix for video files.
#-------------------------------------------------------------------------------
station_number=Cordoba

#-------------------------------------------------------------------------------
# Camera URL. See https://ispyconnect.com/sources.aspx to get your URL. You will
# also need to know the IP address of the camera. Some streams from cameras do
# not startup quickly and sync is a problem. Use the skip_first_seconds variable
# to throw away the first few seconds. Total video time is capture_seconds minus
# skip_first_seconds.
#-------------------------------------------------------------------------------
camUser1="admin"
c1pass="laboratorioh2o"

skip_first_seconds=20
daytime_only="yes"

#-------------------------------------------------------------------------------
# No more variables to edit below here...
#-------------------------------------------------------------------------------


#-------------------------------------------------------------------------------
# Create any folders that might not be there
#-------------------------------------------------------------------------------
time_stamp=`date '+%Y%m%d-%H%M%S'`
date_stamp=`date '+%Y%m%d'`
folder=${video_folder}/${station_number}
image=${folder}/${station_number}_${time_stamp}.jpg

# Setup folder(s).
if [ ! -d ${folder} ]
then
  mkdir -p ${folder}
else
  # Remove any existing videos of the same name as this one.
  if [ -f ${image} ]
  then
    rm -rf ${image}
  fi
fi

#-------------------------------------------------------------------------------
# Capture the image
#-------------------------------------------------------------------------------


echo $camUser1
echo $c1pass
echo $camera1
echo ${skip_first_seconds}
echo ${image}

#/usr/local/bin/ffmpeg -i rtsp://$camUser1:$c1pass@$camera1 -ss ${skip_first_seconds} -vframes 1 ${image}
sudo /usr/local/bin/ffmpeg -i rtsp://$camUser1:$c1pass@$camera1 -ss ${skip_first_seconds} -vframes 1 ${image}

