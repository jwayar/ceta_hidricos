#!/bin/bash
#-------------------------------------------------------------------------------
# capture_video                                                     John E Parks
# 5/17/2017                                                     jeparks@usgs.gov
#-------------------------------------------------------------------------------
# This script can be used to capture video from a camera using the RSTP
# protocol. Most IP cameras support the RSTP protocol but you will need to
# know how to connect to the stream. For connection information, visit
# https://www.ispyconnect.com/sources.aspx, then select your manufacturer
# and model to see the available streams and URLs to download from.
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# The video_folder is the top-level folder where videos recorded from the IP
# camera will be stored. Sub-folders will be created with the date of the
# recording
#-------------------------------------------------------------------------------
video_folder=/home/pi/lspiv/169.254.175.180
camera1=169.254.175.180
#-------------------------------------------------------------------------------
# Station number. Used as a prefix for video files.
#-------------------------------------------------------------------------------
station_number=Cordoba

#-------------------------------------------------------------------------------
# Camera URL. See https://ispyconnect.com/sources.aspx to get your URL. You will
# also need to know the IP address of the camera. Some streams from cameras do
# not startup quickly and sync is a problem. Use the skip_first_seconds variable
# to throw away the first few seconds. Total video time is capture_seconds minus
# skip_first_seconds.
#-------------------------------------------------------------------------------
camUser1="admin"
c1pass="laboratorioh2o"

skip_first_seconds=10
capture_seconds=40
daytime_only="yes"

#-------------------------------------------------------------------------------
# No more variables to edit below here...
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Create any folders that might not be there
#-------------------------------------------------------------------------------
time_stamp=`date '+%Y%m%d-%H%M%S'`
date_stamp=`date '+%Y%m%d'`
#folder=${video_folder}/${station_number}/${date_stamp}
folder=${video_folder}/${station_number}
video=${folder}/${station_number}_${time_stamp}.mp4

# Setup folder(s).
if [ ! -d ${folder} ]
then
  mkdir -p ${folder}
else
  # Remove any existing videos of the same name as this one.
  if [ -f ${video} ]
  then
    rm -rf ${video}
  fi
fi

#-------------------------------------------------------------------------------
# Capture the video
#-------------------------------------------------------------------------------


echo $camUser1
echo $c1pass
echo $camera1
echo ${skip_first_seconds}
echo ${capture_seconds}
echo ${video}

#/usr/local/bin/ffmpeg -i rtsp://$camUser1:$c1pass@$camera1 -ss ${skip_first_seconds} -vcodec copy -t ${capture_seconds} -v quiet ${video}

sudo /usr/local/bin/ffmpeg -i rtsp://$camUser1:$c1pass@$camera1 -ss ${skip_first_seconds} -vcodec copy -t ${capture_seconds}  ${video}
#-------------------------------------------------------------------------------
# Video rotation (optional). This is used to rotate a video when needed. Be
# aware that on a RPi Model 3 B rotating 30 seconds of video can take up to 3
# min. If you are also collecting video from the camera, you may be competing
# for resources. It may be better to rotate the video once it has been offloaded
# from the RPi. Options for the transpose field are:
#
#   0 = 90CounterCLockwise and Vertical Flip (default)
#   1 = 90Clockwise
#   2 = 90CounterClockwise
#   3 = 90Clockwise and Vertical Flip
#   Use -vf "transpose=2,transpose=2" for 180 degrees.
#
# Uncomment the following 2 lines and edit as needed to rotate video after 
# it has been captured.
#-------------------------------------------------------------------------------
# new_name=`echo ${video} | sed 's/\./_rotate\./'`
# /usr/local/bin/ffmpeg -i ${video} -vf "transpose=1" -v quiet ${new_name}

#-------------------------------------------------------------------------------
# EOF
#-------------------------------------------------------------------------------
