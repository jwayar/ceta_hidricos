#!/bin/bash

# Debug
#exec 5> >(logger -t $0)
#BASH_XTRACEFD="5"
#PS4='$LINENO: 'usuarious
#set -x

# Setup variables
HOST=200.16.30.250 #192.168.50.15
USER=antoine
PASSWORD=patalano #usgs2640
FTPLOG=/home/pi/lspiv/ftplogfile
FTP_SUCCESS_MSG="226 Successfully transferred"
BULLCAMPATH="169.254.175.180" #"192.168.50.13"
PATHTOLSPIV="/home/pi/lspiv/169.254.175.180/Cordoba" # "/home/pi/lspiv/192.168.50.13/Canada"

# =====================
# FIRST CAMERA
# Check camera directory for new files, if new, send via ftp
cd $PATHTOLSPIV
# JPEG
count=`ls -1 *.jpg 2>/dev/null | wc -l` #cantidad de archivos en $PATHTOLSPIV
if [ $count != 0 ]; 
then
    ftp -inv $HOST << EOD &> $FTPLOG
    user $USER $PASSWORD
    cd $BULLCAMPATH
    lcd $PATHTOLSPIV/
    bin
    mput *.jpg
    close
    quit
    EOD
    # Check Log remove files
    if fgrep "$FTP_SUCCESS_MSG" $FTPLOG ;
    then
        echo "ftp OK, deleting transferred jpg files"
        sudo rm -f $PATHTOLSPIV/*.jpg
    else
        echo "ftp Error: " $OUT
        # Send email to Antoine
        echo $(cat $FTPLOG) | mail -s "Canada Pi ftp transfer error: bullet cam jpeg" joel.wayar@gmail.com
    fi
fi

#MPEG4
count=`ls -1 *.mp4 2>/dev/null | wc -l`
if [ $count != 0 ]; then
    ftp -inv $HOST << EOD &> $FTPLOG
    user $USER $PASSWORD
    cd $BULLCAMPATH 
    lcd $PATHTOLSPIV/
    bin
    mput *.mp4
    close
    quit
    EOD
    # Check Log remove files
    if fgrep "$FTP_SUCCESS_MSG" $FTPLOG ;then
        echo "ftp OK, deleting transferred mp4 files"
        sudo rm -f $PATHTOLSPIV/*.mp4
        
    else
        echo "ftp Error: "$OUT
        # Send email to  Antoine
        echo $(cat $FTPLOG) | mail -s "Canada Pi ftp transfer error: bullet cam mpeg4" antoine.patalano@gmail.com
    fi
fi


# sftp antoine@200.16.30.250
# cd LSPIV(Joel)/
