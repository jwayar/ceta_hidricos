#!/usr/bin/env python3
#-------------------------------------------------------------------------------
# twilight.py                                                       John E Parks
# 5/17/2017                                                     jeparks@usgs.gov
#-------------------------------------------------------------------------------
# This python script is used to determine if the station is in daylight or
# nighttime. It makes use of the Ephem library to calculate dawn and dusk
# (not surise and sunset). There are three ways to calculate dawn and dusk,
# 1) when the sun is 6 degrees below the horizon which is Civilian Twilight,
# 2) -12 degrees which is Nautical Twilight, and 3) -18 degrees which is
# Astronimical Twilight. This script uses Civilian Twilight which is often
# enough light for a camera to record video.
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Edit the following three lines for the correct values. Use decimal degrees
# and remember to use negative longitudes for the western hemisphere. The
# value for Elevation is in meters.
#-------------------------------------------------------------------------------
Latitude  = -31.418415
Longitude = -64.173165
Elevation = 390 
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Import some libraries...
#-------------------------------------------------------------------------------
import ephem
import datetime
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Create an observer (observers exist at locations)
#-------------------------------------------------------------------------------
location      = ephem.Observer()

#-------------------------------------------------------------------------------
# PyEphem takes and returns only UTC times, so get our time in UTC.
#-------------------------------------------------------------------------------
location.date = datetime.datetime.utcnow()

#-------------------------------------------------------------------------------
# The location we want to compute for...
#-------------------------------------------------------------------------------
location.lat  = str(Latitude)      # Note that lat should be in string format
location.lon  = str(Longitude)     # Note that lon should be in string format
location.elev = Elevation          # In meters

#-------------------------------------------------------------------------------
# Set the pressure and horizon values
#   horizon:      -6 = civil twilight    -12 = nautical    -18 = astronomical
#-------------------------------------------------------------------------------
location.pressure = 0
location.horizon  = str(-6)

#-------------------------------------------------------------------------------
# Get the previous values for dawn and dusk...
#-------------------------------------------------------------------------------
prev_dawn = location.previous_rising  (ephem.Sun(), use_center=True)
prev_dusk = location.previous_setting (ephem.Sun(), use_center=True)

#-------------------------------------------------------------------------------
# If the value returned from previous dusk is before the previous dawn, then
# we know that we are after dawn, but before the next dusk. Return what we
# know...
#-------------------------------------------------------------------------------
if prev_dusk < prev_dawn: 
    print("daytime")
    # print("%s - daytime" % location.date)
else:
    #print("nighttime")
    print("daytime")
    #print("%s - daytime" % location.date)
